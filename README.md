# Support Ticket System

Support Ticket System using MERN (MongoDB, Express.js, React.js, Node.js or Nest.js Project.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js installed (>v18)
- MongoDB installed and running
- React

## Getting Started

To get a local copy up and running, follow these steps:


(Directory Structure)

project-root
|-- backend
    |   |-- src
    |   |   |-- main.ts
    |   |-- vercel.json
|-- 
|-- (other frontend files)
|-- vercel.json

### Backend (Nest.js)

1. Navigate to the `backend` directory:

    ```bash
    cd backend
    ```

2. Install dependencies:

    ```bash
    npm install
    ```



3. Start the backend server:

    ```bash
    npm run start:dev
    ```

### Frontend (React.js)

1. Navigate to the `root` directory:



2. Install dependencies:

    ```bash
    npm install
    ```

3. Set up your environment variables by creating a `.env` file and updating the necessary variables. (use backend port to run locally -->PORT 3003)

4. Start the React development server:

    ```bash
    npm start
    ```

## Usage

Click on Agents / Tickets to begin. With Create Ticket Option you can create a ticket with all attributes and similarly for agents 
Filter the data using the filters given

## Contributing

If you'd like to contribute to this project, please follow these guidelines:

1. Fork the project.
2. Create your feature branch (`git checkout -b feature/YourFeature`).
3. Commit your changes (`git commit -m 'Add some feature'`).
4. Push to the branch (`git push origin feature/YourFeature`).
5. Open a pull request.

 

## Project Link


Project Link:  https://gitlab.com/assignment7850703/tickets

