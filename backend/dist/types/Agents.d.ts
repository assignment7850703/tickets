export declare class Agents {
    name: String;
    email: String;
    phone: String;
    description: String;
    active: Boolean;
    dateCreated: Date;
    ticketCount: Number;
}
