export declare class Tickets {
    topic: String;
    description: String;
    dateCreated: Date;
    severity: String;
    type: String;
    assignedTo?: String;
    status: String;
    resolvedOn?: Date;
}
